![Neural Net](docs/nn.png)
# MY NEURAL NET
Creating a neural network without a ML frame work to identify handwritten digits from the [MNIST Database](https://en.wikipedia.org/wiki/MNIST_database)

## Requirements
- [ ] Python >= 3.6 on your system.
- [ ] It is recommended to use a [virtualenv](https://virtualenv.pypa.io/en/latest/userguide/) for the project.
- [ ] Install requirements with ```pip install -r requirements.txt```

## Get started
in src/main.py you find some predefined script for creating a network, train it with training data and predict some random images of the test data. Just run ```python src/main.py``` to start! Now you can change parameters and follow the effect on cost, accuracy and the result.


## Data
A data set contains an array with images of handwritten digits ```data[0]``` and a array with the correlated numbers ```data[1]```. An image is a row vector with 784 pixels (28x28). 
- training data (50 000):
for training the network.

- validation data (10 000):
to recognize overfitting of the model, new images are used for the model validation.

- test data (10 000):
to further recognize overfitting of the parameters, again new images are used for testing the model.


## Network API

- train(train_data, eta=3, lmbda=0.0, epochs=30, mini_batch_size=10, validation_data=None)
    - train_data: train data set
    - eta: learning rate.
    - lmbda: regularization factor to compensate overfitting.
    - epochs: training epochs.
    - mini_batch_size: training data is splitted into mini_batches to speed up gradient descent.
    - validation_data: Add validation data set for validation of network performance after each training epoch.

- predict(images)
    - images: np.array/list of images. One row per image.
    
- test(test_data)
    - test_data: test data set.



## Tests
cd into the src folder and run
``` python -m pytest ```
