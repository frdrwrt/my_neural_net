from pytest import fixture


@fixture
def data():
    from os.path import dirname, realpath, join
    from data.data import Data
    test_data_file_path = join(dirname(realpath(__file__)), "data", "test", "test_data.pkl.gz")
    return Data(test_data_file_path)


@fixture
def plot():
    from plotting import PlotFactory
    return PlotFactory()
