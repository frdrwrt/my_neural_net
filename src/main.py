from os.path import join, abspath, dirname
from time import sleep

import matplotlib.pyplot as plt
import numpy as np

from data import Data
from network import Network
from plotting import PlotFactory

DATA_PATH = join(dirname(abspath(__file__)), "..", "data", "mnist.pkl.gz")
data = Data(DATA_PATH)
plot = PlotFactory()


def run_training():
    network = Network([784, 30, 10])

    network.train(data.train, eta=0.5, lmbda=0.1, epochs=30, mini_batch_size=10, validation_data=data.validation)
    return network


def run_prediction(network):
    images = [data.random_image() for image in range(9)]
    predictions = network.predict(np.array(images))
    for image, prediction in zip(images, predictions):
        plot.image(image, prediction[0])
        sleep(3)


if __name__ == "__main__":
    network = run_training()
    run_prediction(network)
    plt.waitforbuttonpress()
