from time import sleep

import numpy as np
import pandas as pd


def test_train_progress(plot):
    result = pd.DataFrame(
        {"train_accuracy": [0.78, 0.8, 0.99], "validation_accuracy": [0.76, 0.78, 0.98], "error": [0.8, 0.7, 0.2]})
    conf_matrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]])

    plot.train_progress(result, conf_matrix)

    sleep(1)
    plot.train_progress(result, conf_matrix * 2)
    sleep(3)
