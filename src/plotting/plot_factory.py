from .image import PlotImage
from .train_progress import PlotTrainProgress


class PlotFactory:

    def __init__(self):
        self.plot_image = PlotImage()
        self.plot_train_progress = PlotTrainProgress()

    def image(self, x, prediction):
        """
        plot single image with prediction.
        :param x: one row np.array with all pixels
        :param prediction: number
        """
        self.plot_image.show()
        self.plot_image.update(x, prediction)

    def train_progress(self, result, conf_matrix):
        """
        plot result of training past training epochs and confusion matrix.
        :param result: one row np.array with all pixels
        :param conf_matrix: numpy 2d matrix
        """
        self.plot_train_progress.show()
        self.plot_train_progress.update(result, conf_matrix)
