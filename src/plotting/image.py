import numpy as np

from .plot import Plot


class PlotImage(Plot):

    def update(self, x, prediction):
        height = width = int(np.sqrt(x.shape[1]))
        image = np.reshape(x, (height, width))
        self.axs.imshow(image, cmap='Greys')
        self.axs.set_title(prediction, fontdict={'fontsize': 32})
        self.axs.get_xaxis().set_visible(False)
        self.axs.get_yaxis().set_visible(False)
        self._update()
