import matplotlib.pyplot as plt

from .plot import Plot


class PlotTrainProgress(Plot):

    def create_fig(self):
        self.fig = plt.figure(figsize=(9, 3))
        self.axs = self.fig.subplots(1, 3)

    def update(self, result, conf_matrix):
        for ax in self.axs:
            ax.clear()
        result["error"].plot(ax=self.axs[0])
        self.axs[1].plot(result.loc[:, "train_accuracy": "validation_accuracy"])
        if conf_matrix is not None:
            self.axs[2].imshow(conf_matrix, interpolation="nearest", cmap=plt.cm.Blues)
            self.axs[2].set_title('Confusion Matrix')
        self.axs[0].set_ylabel('error')
        self.axs[0].set_xlabel('epochs')
        self.axs[0].set_title(result["error"].iloc[-1])
        self.axs[1].set_ylabel('accuracy')
        self.axs[1].set_xlabel('epochs')
        self.axs[1].set_title(result["validation_accuracy"].iloc[-1])
        self._update()
