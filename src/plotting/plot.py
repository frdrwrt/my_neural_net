import matplotlib.pyplot as plt


class Plot:

    def __init__(self):
        self.fig = None
        self.axs = None

    def show(self):
        if self.fig is None:
            self.create_fig()
        self._update()

    def _update(self):
        plt.tight_layout()
        plt.pause(0.000001)

    def create_fig(self):
        self.fig = plt.figure()
        self.axs = self.fig.subplots()
