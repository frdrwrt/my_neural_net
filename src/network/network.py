import numpy as np
import pandas as pd

from data import DataService
from plotting import PlotFactory
from .cost_functions import EntropyCost
from .evaluation import confusion_matrix, accuracy_score
from .math_functions import sigmoid_prime, sigmoid


class Network(object):

    def __init__(self, layers, cost=EntropyCost):
        self.num_layers = len(layers)
        self.layers = layers
        self.biases = [np.random.randn(m, 1) for m in layers[1:]]
        self.weights = [np.random.randn(m, n) for n, m in zip(layers[:-1], layers[1:])]
        self.cost = cost
        self.plot = PlotFactory()
        self.results = pd.DataFrame(columns=['error', 'train_accuracy', 'validation_accuracy'])

    def predict(self, images):
        prediction = [self.__feed_forward(x) for x in images]
        prediction = np.array(prediction).reshape(images.shape[0], 10)
        prediction = DataService.numerize(prediction)
        return prediction

    def test(self, test_data):

        y_predict = self.predict(test_data[0])
        return confusion_matrix(y_predict, test_data[1]), accuracy_score(y_predict, test_data[1])

    def train(self, train_data, eta=3, lmbda=0.0, epochs=30, mini_batch_size=10, validation_data=None):
        n = train_data[0].shape[0]
        m = mini_batch_size
        train_data[1] = DataService.vectorize(train_data[1])
        for epoch in range(epochs):
            error_epoch = []
            mini_batches = self.__prepare_mini_batches(train_data, n, m)
            for mini_batch in mini_batches:
                error_mini_batch = self.__train_with_mini_batch(mini_batch, eta, lmbda, m, n)
                error_epoch.append(error_mini_batch)
            self.__evaluate_epoch(error_epoch, validation_data, train_data)

    def __prepare_mini_batches(self, train_data, examples, m):
        train_data = DataService.shuffle(train_data)
        return [DataService.split(train_data, e, e + m) for e in range(0, examples, m)]

    def __evaluate_epoch(self, error_epoch, validation_data, train_data):
        epoch = self.results.shape[0] + 1
        amount_train_data = int(train_data[0].shape[0] * 0.1 + 1)
        train_data = DataService.shuffle(train_data)
        train_data = DataService.split(train_data, 0, amount_train_data)
        train_data[1] = DataService.numerize(train_data[1])
        train_conf_matrix, train_accuracy = self.test(train_data)
        if validation_data:
            validation_conf_matrix, validation_accuracy = self.test(validation_data)
        else:
            validation_conf_matrix, validation_accuracy = (None, None)
        self.results.loc[epoch] = [np.mean(error_epoch), train_accuracy, validation_accuracy]
        self.plot.train_progress(self.results, validation_conf_matrix)

    def __train_with_mini_batch(self, mini_batch, eta, lmbda, m, n):
        nabla_b, nabla_w = self.__get_empty_b_and_w()
        error_mini_batch = []
        for example in range(0, m):
            delta_nabla_b, delta_nabla_w, average_error = self.__backpropagation(mini_batch[0][example],
                                                                                 mini_batch[1][example])
            nabla_b = self.__update_parameters(nabla_b, delta_nabla_b)
            nabla_w = self.__update_parameters(nabla_w, delta_nabla_w)
            error_mini_batch.append(average_error)
        self.__update_weights_and_biases(nabla_w, nabla_b, eta, lmbda, m, n)
        return np.mean(error_mini_batch)

    def __backpropagation(self, x, y):

        # empty result matrix of nabla_b and nabla_w
        delta_nabla_b, delta_nabla_w = self.__get_empty_b_and_w()

        # Feedforward for getting z and activations of all layers:
        x.shape = (x.shape[0], 1)
        y.shape = (y.shape[0], 1)
        activations = [x]
        zs = []
        for w, b in zip(self.weights, self.biases):
            activations.append(self.__calc_a(w, activations[-1], b))
            zs.append(self.__calc_z(w, activations[-2], b))

        # Backpropagation of the error
        # l is layer number from back to front
        error = 0
        average_error = 0
        for l in range(1, self.num_layers):
            if l == 1:
                # last layer
                error = self.cost.delta(zs[-1], activations[-1], y)
                average_error = np.mean(error)
            else:
                # all other layers
                error = np.dot(self.weights[-l + 1].transpose(), error) * sigmoid_prime(zs[-l])

            delta_nabla_b[-l] = error
            delta_nabla_w[-l] = np.dot(error, activations[-l - 1].transpose())

        return (delta_nabla_b, delta_nabla_w, average_error)

    def __update_weights_and_biases(self, nabla_w, nabla_b, eta, lmbda, m, n):
        self.weights = [(1 - eta * (lmbda / n)) * w - (eta / m) * nw for w, nw in zip(self.weights, nabla_w)]
        self.biases = [b - (eta / m) * nb for b, nb in zip(self.biases, nabla_b)]

    def __update_parameters(self, parameter, delta_parameter):
        return [p + dp for p, dp in zip(parameter, delta_parameter)]

    def __get_empty_b_and_w(self):
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]
        return (nabla_b, nabla_w)

    def __feed_forward(self, a):
        a = DataService.check_shape(a)
        for w, b in zip(self.weights, self.biases):
            a = self.__calc_a(w, a, b)
        return a

    @staticmethod
    def __calc_z(w, a, b):
        return np.dot(w, a) + b

    @staticmethod
    def __calc_a(w, a, b):
        return sigmoid(Network.__calc_z(w, a, b))
