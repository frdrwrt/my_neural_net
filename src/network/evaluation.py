import numpy as np


def confusion_matrix(y_true, y_pred):
    ys = np.concatenate((y_true, y_pred))
    dim = ys.max() + 1
    matrix = np.zeros((dim, dim), dtype=int)
    for true, pred in zip(y_true, y_pred):
        matrix[int(true)][int(pred)] += 1
    return matrix


def accuracy_score(y_true, y_predict):
    correct = np.sum(y_true == y_predict)
    return correct / y_true.shape[0]
