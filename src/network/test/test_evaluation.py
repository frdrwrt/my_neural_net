import numpy as np

from network.evaluation import accuracy_score, confusion_matrix

y_true = np.array([0, 1, 2, 2, 1, 3]).reshape((6, 1))
y_pred = np.array([0, 1, 0, 2, 1, 1]).reshape((6, 1))


def test_confusion_matrix():
    matrix = confusion_matrix(y_true, y_pred)

    assert matrix.shape == (4, 4)
    assert (matrix == np.array([(1, 0, 0, 0),
                                (0, 2, 0, 0),
                                (1, 0, 1, 0),
                                (0, 1, 0, 0)])
            ).all()


def test_accuracy_score():
    score = accuracy_score(y_true, y_pred)
    assert score == 4 / 6
