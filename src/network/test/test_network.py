from pytest import fixture


@fixture()
def network():
    from network import Network
    return Network((784, 2, 10))


def test_network(network):
    assert network.num_layers == 3
    assert network.biases[0].shape == (2, 1)
    assert network.biases[1].shape == (10, 1)
    assert network.weights[0].shape == (2, 784)
    assert network.weights[1].shape == (10, 2)


def test_training(network, data):
    network.train(data.train, epochs=2, mini_batch_size=1, validation_data=data.validation)


def test_predict(network, data):
    result = network.predict(data.test[0])
    assert result.size == 4


def test_test(network, data):
    network.test(data.test)
