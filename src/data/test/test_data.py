def test_random_image(data):
    image = data.random_image()
    assert image.shape == (1, 784)


def test_data(data):
    assert len(data.train) == 2
    assert len(data.validation) == 2
    assert len(data.test) == 2
    assert data.train[0].shape == (4, 784)
    assert data.validation[0].shape == (4, 784)
    assert data.test[0].shape == (4, 784)
    assert data.train[1].shape == (4, 1)
    assert data.validation[1].shape == (4, 1)
    assert data.test[1].shape == (4, 1)
