import numpy as np

from data.data_service import DataService


def test_shuffle(data):
    shuffled = DataService.shuffle(data.train)
    assert shuffled[1].size == 4


def test_split(data):
    splitted = DataService.split(data.train, 0, 2)
    assert splitted[1].shape == (2, 1)


def test_vectorize():
    vector = DataService.vectorize(np.array([[1], [2]]))
    assert (vector[1] == np.array([0, 0, 1, 0, 0, 0, 0, 0, 0, 0])).all()


def test_numerize():
    number = DataService.numerize(np.array([[0, 0, 1, 0, 0, 0, 0, 0, 0, 0]]))
    assert number == np.array([[2]])


def test_check_shap():
    row = np.array([0, 1, 2, 3, 4, 5, 6])
    row = DataService.check_shape(row)
    assert row.shape == (7, 1)

    row = np.reshape(row, (1, 7))
    row = DataService.check_shape(row)
    assert row.shape == (7, 1)
