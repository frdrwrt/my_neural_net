import gzip
import pickle

import numpy as np


class DataService:

    @staticmethod
    def load_mnist(path):
        with gzip.open(path, 'rb') as f:
            training_data, validation_data, test_data = \
                pickle.load(f, encoding='bytes')
        return training_data, validation_data, test_data

    @staticmethod
    def load_data(path):
        for data in DataService.load_mnist(path):
            yield [data[0], np.reshape(data[1], (data[1].shape[0], 1))]

    @staticmethod
    def shuffle(data):
        import random
        shapes = [d.shape for d in data]
        combined = np.concatenate(data, axis=1)
        random.shuffle(combined)
        r = 0
        res = []
        for shape in shapes:
            res.append(combined[:, r:shape[1] + r])
            r += shape[1]
        return res

    @staticmethod
    def split(data, start=0, end=-1):
        return [data[0][start:end], data[1][start:end]]

    @staticmethod
    def vectorize(array):
        def vect(row):
            vec = [0 for i in range(0, 10)]
            vec[int(row[0])] = 1
            return vec

        return np.apply_along_axis(vect, axis=1, arr=array)

    @staticmethod
    def numerize(array):
        res = np.apply_along_axis(np.argmax, axis=1, arr=array)
        return res.reshape(array.shape[0], 1)

    @staticmethod
    def check_shape(row):
        return np.reshape(row, (row.size, 1))
