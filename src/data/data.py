from random import randint

import numpy as np

from .data_service import DataService


class Data:
    def __init__(self, path):
        self.path = path
        self.train, self.validation, self.test = DataService.load_data(path)

    def random_image(self):
        images = self.train[0]
        r = randint(0, len(images) - 1)
        return np.reshape(images[r], (1, 784))
